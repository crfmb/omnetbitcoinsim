/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinApp
 * DESC: Child class of the bitcoin app. The 'new' app handles downloading bitcoin block data from the source app.
 * This node is meant to behave like a newly installed bitcoin node with only the genesis block in its memory.
 * This node follows the headers-first IBD found on the bitcoin P2P protocl page.
 */

#ifndef SRC_BITCOINNEWAPP_H_
#define SRC_BITCOINNEWAPP_H_

#include "bitcoinApp.h"
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>

namespace inet{

class bitcoinNewApp : public bitcoinApp {

public:
    // Constructor/Destructor
    bitcoinNewApp();
    virtual ~bitcoinNewApp();
    // Overriding Functions
    void virtual initialize(int stage) override;
    void virtual sendPacket() override;
    void virtual processPacket(Packet *pk) override;
    // Custom internal functions for client operations.
    void virtual processHeaders();
    void virtual processTx();
    void virtual checkSourceHeight();
    void virtual processGetData();

    int nodeID;

    // Storage Vars
    bool needHeaders;

    /********** Booleans to affect 'script' ******************/
    // New Booleans
    // Sending Bools
    bool sentGetHeaders;
    bool sentGetData;
    bool isFinished;

    // Recieving Bools
    bool gotHeaders;
    bool gotTxnData;

    /************** Objects for sending objects*****************/
    // New Sending Objects
    getDataObj *getDataRqst;
    getheadersObj *getHeaderRqst;

    // New Receiving Objects
    txnObj *receivedTxn;
    headerObj *receivedHeaders;
    std::vector<block_struct> newHeaders;
    std::vector<block_struct> newBlocks; // represents new node memory
    std::vector<std::vector<inet::tx_struct>> newTxMetaData;

};

}

#endif /* SRC_BITCOINNEWAPP_H_ */
