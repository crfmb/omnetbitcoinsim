/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: txnObj
 * DESC: Source file for TxnObj.
 */

#include "txnObj.h"
namespace omnetpp{

txnObj::txnObj() {
}

txnObj::txnObj(const char *newname): bitcoinMsgObj(newname, "TXN"){
// No need to initialize vars, will be setup in bitcoinSourceApp.
}

txnObj::~txnObj() {
}

}

