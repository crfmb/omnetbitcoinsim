/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getdataObj
 * DESC: Source file for getdataobj.
 */

#include "getDataObj.h"

namespace omnetpp{
getDataObj::getDataObj() {
}

getDataObj::getDataObj(const char *newname, int& newcount, std::vector<inet::inv_struct>& newInv): bitcoinMsgObj(newname, "GETDATA"){

    // Initialize count and inventory structs.
    this->count = newcount;
    this->inventory = newInv;

}

getDataObj::~getDataObj() {
}

};

