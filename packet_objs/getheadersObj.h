/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getheaderObj
 * DESC: Used to send the getHeaders packet to the source application.
 */

#ifndef SRC_PACKET_OBJS_GETHEADERSOBJ_H_
#define SRC_PACKET_OBJS_GETHEADERSOBJ_H_
#include "bitcoinMsgObj.h"
#include <string>

namespace omnetpp{

class getheadersObj: public bitcoinMsgObj {
public:
    getheadersObj();
    virtual ~getheadersObj();
    virtual getheadersObj *dup() const override {return new getheadersObj(*this);}
    getheadersObj(const char *newname, int hashcnt, std::string hashstop);
    virtual const char *getName() const override {return this->name;}

    // Declare vars
    std::string hash_stop;
    std::string locater_hashes; // Wont be needed since sending one getheaders with hash_stop
    int hash_count;

};

};

#endif /* SRC_PACKET_OBJS_GETHEADERSOBJ_H_ */
