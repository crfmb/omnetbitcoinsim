/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getdataObj
 * DESC: Used to send the getdata packet to the source application.
 */

#include "bitcoinMsgObj.h"

#ifndef SRC_PACKET_OBJS_GETDATAOBJ_H_
#define SRC_PACKET_OBJS_GETDATAOBJ_H_

namespace omnetpp{

class getDataObj : public bitcoinMsgObj {
public:
    getDataObj();
    virtual ~getDataObj();
    virtual getDataObj *dup() const override {return new getDataObj(*this);}
    getDataObj(const char *newname, int& newcount, std::vector<inet::inv_struct>& newInv);
    virtual const char *getName() const override {return this->name;}

    // Vars, See bitcoinMsgObj for inv_struct definition.
    int count;
    std::vector<inet::inv_struct> inventory;

};

};

#endif /* SRC_PACKET_OBJS_GETDATAOBJ_H_ */
