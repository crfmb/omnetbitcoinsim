/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getdataObj
 * DESC: Source file for bitcoinMsgObj.
 */

#include "bitcoinMsgObj.h"

namespace omnetpp {

bitcoinMsgObj::bitcoinMsgObj () {
}

bitcoinMsgObj ::bitcoinMsgObj (const char *newname, char command []) {

    // Setup message vars
    this->name = newname;
    this->magic = 0xD9B4BEF9; // main network magic value
    this->command = command;
    this->length = 1; // Length not needed for omnet++ handling
    this->checksum = 1; // Normally first 4 bytes of sha256(sha256(payload))
    this->payload = 'a'; // Payload instead stored as separate child object for omnet++ parsing

}

bitcoinMsgObj::~bitcoinMsgObj () {
}

} /* namespace inet */
