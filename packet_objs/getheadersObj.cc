/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getheaderObj
 * DESC: Source file for getheaderObj.
 */

#include "getheadersObj.h"

namespace omnetpp{

getheadersObj::getheadersObj() {
    // TODO Auto-generated constructor stub

}

getheadersObj::~getheadersObj() {
    // TODO Auto-generated destructor stub
}

getheadersObj::getheadersObj(const char *newname, int hashcnt, std::string hashstop): bitcoinMsgObj(newname, "GETHEADERS"){

    this->hash_count = hashcnt;
    this->hash_stop = hashstop;

}

}
