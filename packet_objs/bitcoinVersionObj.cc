/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getdataObj
 * DESC: Source file for bitcoinVersionObj.
 */

#include "bitcoinVersionObj.h"

namespace omnetpp {

bitcoinVersionObj::bitcoinVersionObj() {
}

bitcoinVersionObj::bitcoinVersionObj(const char *newname, int start_height): bitcoinMsgObj(newname, "MESSAGE"){

    // Setup version vars, addresses handles by omnetpp.
    this->version = 0x01;       // Assume 1 for simulation...
    this->services = 0x0001;    // Just NODE_NETWORK (may alter later)
    this->timestamp = 0;        // Leave at 0 for simulation.
    this->nonce = 200;          // Random node nonce (may alter)
    this->start_height = start_height;     // New client only has origin block
    this->relay = false;        // Should client announce relayed transactions?

}

bitcoinVersionObj::~bitcoinVersionObj() {
}

}
