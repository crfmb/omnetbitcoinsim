/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: headerObj
 * DESC: Used to send header information to the new app from source app.
 */

#ifndef SRC_PACKET_OBJS_HEADEROBJ_H_
#define SRC_PACKET_OBJS_HEADEROBJ_H_
#include "bitcoinMsgObj.h"
#include <string.h>

namespace omnetpp{

class headerObj : public bitcoinMsgObj {
public:
    headerObj();
    virtual ~headerObj();
    virtual headerObj *dup() const override {return new headerObj(*this);}
    headerObj(const char *newname, int& count, std::vector<inet::block_struct>& inHeaders);
    virtual const char *getName() const override {return this->name;}

    //Initialize vars
    int count;
    std::vector<inet::block_struct> transferHeaders;

};

};

#endif /* SRC_PACKET_OBJS_HEADEROBJ_H_ */
