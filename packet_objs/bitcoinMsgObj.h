/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: bitcoinMsgObj
 * DESC: Base class for all bitcoin objects being sent through packets (bitcoin wiki specifys that all bitcoin messages are built on bitcoinMsgObj).
 * This class is considered the most "basic" bitcoin message object. Also, this file also has the unique 'storage' structs for each of the bitcoin apps.
 */

#ifndef SRC_BITCOINMSGOBJ_H_
#define SRC_BITCOINMSGOBJ_H_
#include "omnetpp/cobject.h"

#include <string.h>
#include <vector>

namespace inet{

// Main storage object for bitcoin block data.
struct block_struct{

    int version = 1; // for simulation only
    std::string prev_block;
    std::string merkle_root;
    std::string curr_hash;
    int timestamp;
    std::string bits; // bits used to calculate target in python code
    int nonce;
    int txn_count = 2; // 0 for block headers only, 2 for blocks in simulation only
    std::vector<std::string> txns;

};

// Used by the getData object to ask the source app for trxn data.
struct inv_struct{

    int type = 2; // MSG_BLOCK type (for simulation only)
    std::string hash; // object hash

};

// Pseudo version of the transaction struct in Bitcoin. Simplified to make coding easier. Missing features outside project scope.
struct tx_struct{

    int version = 1;
    // Omit flag, no witness data
    int tx_incnt = 1;
    std::string tx_in_out; // Combined tx_in_out for example purposes
    int tx_outcnt = 1;
    // No witness data or lock time

};

}

namespace omnetpp {

class bitcoinMsgObj : public SIM_API cObject {

public:
    bitcoinMsgObj ();
    ~bitcoinMsgObj ();
    virtual bitcoinMsgObj  *dup() const override {return new bitcoinMsgObj (*this);}
    bitcoinMsgObj (const char *newname, char command []) ;
    virtual const char *getName() const override {return this->name;}

    // Basic message parameters. Child classes add their own as well.
    const char* name; // Name for cArray to refer to
    int magic;
    std::string command;
    int length;
    int checksum;
    char payload;

};

}

#endif /* SRC_BITCOINMSGOBJ_H_ */
