/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: headerObj
 * DESC: Source file for headerObj.
 */

#include "headerObj.h"

namespace omnetpp{

headerObj::headerObj() {
}

headerObj::headerObj(const char *newname, int& count, std::vector<inet::block_struct>& inHeaders): bitcoinMsgObj(newname, "HEADER"){

    // Setup header count and block headers. (see bitcoin wiki documentation)
    this->count = count;
    this->transferHeaders = inHeaders;

}

headerObj::~headerObj() {
}

}

