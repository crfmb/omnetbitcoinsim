/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: getdataObj
 * DESC: Used by each application to send their version to the other app.
 */

#ifndef SRC_BITCOINVERSIONOBJ_H_
#define SRC_BITCOINVERSIONOBJ_H_

#include "bitcoinMsgObj.h"

namespace omnetpp {

class bitcoinVersionObj : public bitcoinMsgObj{
public:
    // Funcs
    bitcoinVersionObj();
    virtual ~bitcoinVersionObj();
    virtual bitcoinVersionObj *dup() const override {return new bitcoinVersionObj(*this);}
    bitcoinVersionObj(const char *newname, int start_height);
    virtual const char *getName() const override {return this->name;}

    // Version parameters from bitcoin wiki... (source and destination addresses handled by omnet++)
    int version;
    int services;
    int timestamp;
    int nonce;
    int start_height;
    bool relay;
    int nodeID;

};

}

#endif /* SRC_BITCOINVERSIONOBJ_H_ */
