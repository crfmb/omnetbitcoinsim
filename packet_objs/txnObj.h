/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: txnObj
 * DESC: Used to send trxn data to the new bitcoin app when source app receives getData.
 */

#ifndef SRC_PACKET_OBJS_TXNOBJ_H_
#define SRC_PACKET_OBJS_TXNOBJ_H_
#include "bitcoinMsgObj.h"

namespace omnetpp{

class txnObj : public bitcoinMsgObj {
public:
    txnObj();
    virtual ~txnObj();
    virtual txnObj *dup() const override {return new txnObj(*this);}
    txnObj(const char *newname);
    virtual const char *getName() const override {return this->name;}

    //Used Vectors for easier programming
    std::vector<std::string> trxHashes; // Normally hashes aren't sent back and forth, this was done for ease of indexing.
    std::vector<std::vector<inet::tx_struct>> trxnsperblock;

};

};
#endif /* SRC_PACKET_OBJS_TXNOBJ_H_ */
