/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinApp
 * DESC: This class is a custom Bitcoin application for Omnet++, this app closely replicates the Bitcoin protocol
 * found in the real-world Bitcoin application for the headers-first IBD. Some liberties were taken for ease of programming
 * and to make the Bitcoin protocol work with the omnet++ packet system. This class serves as the base class for bitcoinNewApp
 * and bitcoinSourceApp, containing shared variables between the classes or organization purposes. A majority of the main 'script'
 * used to replicate the headers-first IBD can be found in bitcoinSourceApp and bitcoinNewApp. The source for the original
 * Bitcoin protocol can be found in these links:
 *
 * https://en.bitcoin.it/wiki/Protocol_documentation
 * https://bitcoin.org/en/p2p-network-guide
 *
 * Please contact Charles Rawlins at (crfmb@mst.edu) for additional questions.
 */

#ifndef __MSTSPRING2020RESEARCH_BITCOINAPP_H_
#define __MSTSPRING2020RESEARCH_BITCOINAPP_H_

// Omnet++ files include:
#include <omnetpp.h>
#include <string.h>
#include "inet/applications/udpapp/UdpBasicApp.h"
#include "inet/common/INETDefs.h"
#include "inet/applications/base/ApplicationPacket_m.h"
#include "inet/common/ModuleAccess.h"
#include "inet/common/TagBase_m.h"
#include "inet/common/TimeTag_m.h"
#include "inet/common/packet/Packet.h"
#include "inet/networklayer/common/FragmentationTag_m.h"
#include "inet/networklayer/common/L3AddressResolver.h"
#include "inet/transportlayer/contract/udp/UdpControlInfo_m.h"
// Custom files include:
#include "src/packet_objs/bitcoinVersionObj.h"
#include "src/packet_objs/bitcoinMsgObj.h"
#include "inet/applications/base/ApplicationPacket_m.h"
#include "src/packet_objs/getheadersObj.h"
#include "src/packet_objs/headerObj.h"
#include "src/packet_objs/getDataObj.h"
#include "src/packet_objs/txnObj.h"

namespace inet{

// Main class for the Bitcoin Client Application, parent to bitcoinNewApp and bitcoinSourceApp.
// Inherits from omnet++ UdpBasicApp
class bitcoinApp : public INET_API UdpBasicApp{

public:
    bitcoinApp();
    virtual ~bitcoinApp();

    bool sentInitMsg = false;

    // CUSTOM FUNCTIONS
    // -> custom functions handled by children

    //Shared storage Vars
    int currentBlockHeight;
    std::string genesisHash;
    std::string genesisStr;

    /********** Share Booleans to affect 'script' ******************/
    // Universal Booleans
    // Sending Bools
    bool sentVer;
    bool sentVerAck;
    // Recieving Bools
    bool gotVer;
    bool gotVerAck;

    /************** Objects for sending objects*****************/
    // Universal Sending Objects
    bitcoinVersionObj *versionObj;
    bitcoinMsgObj *verAck;

    // Universal Receiving Objects
    bitcoinVersionObj receivedVer;

};

}

#endif
