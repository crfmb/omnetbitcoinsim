/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinParentApp
 * DESC: Child class of the bitcoin app. The 'source' app behaves like a source of Bitcoin information for the single 'new' app.
 * The source app reads in a certain amount of blocks before it gives header/data requests to the 'new' app.
 */

#ifndef SRC_BITCOINSOURCEAPP_H_
#define SRC_BITCOINSOURCEAPP_H_

// Parent Header
#include "src/bitcoinApp.h"
#include "src/bitcoinNewApp.h"
// include csv file reader headers
#include <iostream>
#include <fstream>
#include <vector>
#include <iterator>
#include <string>
#include <algorithm>
#include <queue>

namespace inet {

class bitcoinSourceApp : public bitcoinApp {
private:
    // Custom Functions
    void virtual getBlockchainData();
    void virtual processGetHeaders();
    void virtual processGetData();
    bool virtual checkIfFinished();
    void virtual resetDeliveryLoop();

public:
    // Constructor/Destructor
    bitcoinSourceApp();
    virtual ~bitcoinSourceApp();
    // Overriding functions
    void virtual initialize(int stage) override;
    void virtual sendPacket() override;
    void virtual processPacket(Packet *pk) override;

    //Vars for handling multiple new nodes.
    int numToServ = destAddresses.size();
    int currServIdx = 0;
    L3Address workingAddr;
    bool receivedFirstVer;
    std::queue<int> serveQueue;
    bool isWaitingForFirst;
    bool firstTimeLoop;
    bool servingFirstNode;

    /********** Booleans to affect 'script' ******************/
    // Source Booleans
    // Sending Bools
    bool sentHeaders;
    bool sentGetData;
    bool sentVer;
    bool sendVerAck;

    // Receiving Bools
    bool gotGetHeaders;
    bool gotGetData;
    bool isFinished;

    // Storage Objects
    headerObj *headerDataObj;
    txnObj *sendTxnObj;

    /************** Objects for sending objects*****************/
    // Source Sending Objects
    std::vector<block_struct> sourceHeaders;
    std::vector<block_struct> sourceBlocks;

    // Source Receiving Objects
    getheadersObj receivedGetHeaders;
    getDataObj receivedGetData;
    int numHeadersSend;

};

}
#endif /* SRC_BITCOINSOURCEAPP_H_ */
