/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinApp
 * DESC: Source File for BitcoinNewApp.
 */

#include "bitcoinNewApp.h"

namespace inet{

Define_Module(bitcoinNewApp);

bitcoinNewApp::bitcoinNewApp() {

    // Setup new node app bitcoin data (genesis block, etc.)
    this->currentBlockHeight = 0;
    genesisStr = "0000000000000000000000000000000000000000000000000000000000000000"; //Genesis hash from Bitcoin
    this->genesisHash = genesisStr;
    this->genesisStr = "GENESIS";

    // Setup Version broadcasting
    this->versionObj = new bitcoinVersionObj("version", 1);

    char verackcmd [] = "verack";
    this->verAck = new bitcoinMsgObj("verack", verackcmd);
    this->sentVer = false;
    this->gotVer = false;
    this->sentVerAck = false;
    this->gotVerAck = false;

    // Send getheaders to get block hashes from source
    this->needHeaders = false;
    this->sentGetHeaders = false;

    // Should recieve headers packet and process data
    this->gotHeaders = false;

    // New node should send getblocks once headers have been recieved
    this->sentGetData = false;

    // Should process blocks once recieved
    this->gotTxnData = false;
    this->isFinished = false;

}

bitcoinNewApp::~bitcoinNewApp() {
}

// Initializes application and Bitcoin client
void bitcoinNewApp::initialize(int stage){

    UdpBasicApp::initialize(stage);

    // Setup node ID for multiple host handling
    this->nodeID = par("newAppID");
//    socket.setOutputGate(gate("socketOut"));
//    socket.bind(localPort);
//    setSocketOptions();


}

void bitcoinNewApp::sendPacket(){

    // Setup data packet

    std::ostringstream str;
    str << packetName << "-" << numSent;
    Packet *packet = new Packet(str.str().c_str());
    if(dontFragment)
        packet->addTag<FragmentationReq>()->setDontFragment(true);

    const auto& payload = makeShared<ApplicationPacket>();
    payload->setChunkLength(B(par("messageLength")));
    payload->addTag<CreationTimeTag>()->setCreationTime(simTime());
    packet->insertAtBack(payload);

    // End Setup data packet

    // Begin packet data deciding logic

    // Use infinite while loop to create 'script'
    while(true){

        bool hasDatatoSend = false; // Bool for catchall

        // Send dummy msg to initialize socket
        if(!this->sentInitMsg){
            this->sentInitMsg = true;
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("WaitMsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "NODE IS INITIALIZING!" << endl;
            break;
        }

        // Send version object initially
        if(!this->sentVer){
            this->sentVer = true;
            hasDatatoSend = true;
            this->versionObj->nodeID = this->nodeID;
            packet->addObject(this->versionObj);
            EV_INFO << "SENDING VERSION OBJ..."<< endl;
            break;
        }

        // Send verack after getting version packet
        if(!this->sentVerAck && this->gotVer){
            this->sentVerAck = true;
            hasDatatoSend = true;
            packet->addObject(this->verAck);
            EV_INFO << "SENDING VERSION ACK..."<< endl;
            break;
        }

        // Handle getheaders
        if(this->needHeaders && !this->sentGetHeaders){
            this->sentGetHeaders = true;
            hasDatatoSend = true;
            packet->addObject(this->getHeaderRqst);
            EV_INFO << "SENDING GETHEADERS!" << endl;
            break;

        }

        // Handle getHeaders
        if(this->gotHeaders && !this->sentGetData){
            this->sentGetData = true;
            hasDatatoSend = true;
            processGetData();
            packet->addObject(this->getDataRqst);
            EV_INFO << "SENDING GETDATA!" << endl;
            break;
        }

        // Send finished message infinitely once script is done.
        if(this->isFinished){
            char cmdMsg [] = "finish";
            bitcoinMsgObj *finishMsg = new bitcoinMsgObj("isfinished", cmdMsg);
            packet->addObject(finishMsg);
            EV_INFO << "NODE IS FINISHED!" << endl;
            break;

        }

        // Catchall object to prevent omnet++ from crashing if something goes wrong...
        if(!hasDatatoSend){
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("waitmsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "NODE IS WAITING!" << endl;
            break;
        }

    }

    // End sending packet logic...

    // Get destination address from .ini file and send packet

    L3Address destAddr = chooseDestAddr();
    emit(packetSentSignal, packet);
    socket.sendTo(packet, destAddr, destPort);
    numSent++;

    // Print out success string to simulation monitor
    EV_INFO << "NEW NODE SENT DATA!" << endl;

}

// Called when packets are received...
void bitcoinNewApp::processPacket(Packet *pk){

    // Begin extracting objects from recieved packet
    while(true){

        /************NEW NODE SCRIPT****************/
        cObject *temp = pk->getOwner();
        if(pk->hasObject("WaitMsg")){
            EV_INFO << "GOT WAIT MSG! DELETING" <<  endl;
            break;
        }

        if(pk->hasObject("isfinished")){
            EV_INFO << "GOT FINISHED SIGNAL! DELETING" <<  endl;
            break;
        }

        // Handle receiving txn packet
        if(pk->hasObject("txn")){
            this->gotTxnData = true;
            this->nodeID; // Temp
            EV_INFO << "RECEIVED TXN DATA FROM SOURCE!" << endl;
            cObject *tempObj = pk->getObject("txn");
            txnObj *newTxns = check_and_cast<txnObj *>(tempObj);
            this->receivedTxn = newTxns;
            processTx();
            this->isFinished = true;
            cDisplayString& displayString = getParentModule()->getDisplayString();
            displayString.setTagArg("i", 0, "misc/sun");
            displayString.setTagArg("is", 1, "s");
            break;
        }

        // Handle receiving headers packet
        if(pk->hasObject("headers")){
            this->gotHeaders = true;
            EV_INFO << "RECEIVED HEADERS FROM SOURCE!" << endl;
            cObject *tempObj = pk->getObject("headers");
            headerObj *newheaders = check_and_cast<headerObj *>(tempObj);
            this->receivedHeaders = newheaders;
            processHeaders();
            break;
        }

        // Handle receiving verack packet.
        if(pk->hasObject("verack")){
            this->gotVerAck = true;
            EV_INFO << "RECEIVED VERACK FROM SOURCE" << endl;
            // Check if new app is up to date.
            break;
        }

        if(this->nodeID == 1){

            EV_INFO << "Temp" << endl;
        }

        // Handle receiving version packet.
        if(pk->hasObject("version")){
            cObject *tempObj = pk->getObject("version");
            bitcoinVersionObj *version = check_and_cast<bitcoinVersionObj *>(tempObj);
            EV_INFO << "RECEIVED VERSION BLOCK: start_height = " << version->start_height <<  endl;
            this->receivedVer = *version;
            this->gotVer = true;
            checkSourceHeight();
            break;
        }else{ // Received weird packet...
            EV_INFO << "SOMETHING WEIRD HAPPENED" << endl;
            break;
        }


    }

    // END HANDLING FOR INCOMING OBJECTS

    EV_INFO << "NEW NODE RECEIVED DATA!" << endl;

    emit(packetReceivedSignal, pk);
    delete pk;
    numReceived++;

}

/*************** Private Functions************/

void bitcoinNewApp::checkSourceHeight(){

    this->currentBlockHeight = this->receivedVer.start_height;
    if(this->receivedVer.start_height >= this->currentBlockHeight ){
        this->needHeaders = true;
        this->getHeaderRqst = new getheadersObj("getheaders", this->receivedVer.start_height - this->currentBlockHeight, this->genesisHash);
    }

}

// Process and store incoming Header information
void bitcoinNewApp::processHeaders(){

    int i;
    for(i = 0; i < this->receivedHeaders->transferHeaders.size(); i++){
        this->newHeaders.push_back(this->receivedHeaders->transferHeaders[i]);
    }

    // New Node should have all headers!
    // Node now needs to get txn data. (from p2p protocol)

}

void bitcoinNewApp::processGetData(){

    // Send request for txn data for all current blocks (simulation only)
    int numBlocksNeed = this->newHeaders.size();
    struct inv_struct workingInvStruct;
    std::vector<inv_struct> tempVector;

    int i;
    for(i = 0; i < numBlocksNeed; i++){
        workingInvStruct.hash = newHeaders[i].curr_hash;
        tempVector.push_back(workingInvStruct);
    }

    this->getDataRqst = new getDataObj("getdata",numBlocksNeed, tempVector);

}

// Process and store incoming Block information
void bitcoinNewApp::processTx(){

    // Parse metadata
    this->newTxMetaData = this->receivedTxn->trxnsperblock;
    this->newBlocks = this->newHeaders;

    // Fill in missing transaction info in new app blocks
    std::string workString;
    std::vector<std::string> workingStringVec;
    int i,j;
    for(i = 0; i < this->newBlocks.size();i++){

        workingStringVec.clear();

        for(j = 0;j < this->receivedTxn->trxHashes.size();j++){

            if(this->receivedTxn->trxHashes[j].compare(this->newBlocks[i].curr_hash) == 0){
                workString = this->receivedTxn->trxnsperblock[j][0].tx_in_out;
                workingStringVec.push_back(workString);
                workString = this->receivedTxn->trxnsperblock[j][1].tx_in_out;
                workingStringVec.push_back(workString);
                this->newBlocks[i].txn_count = receivedTxn->trxnsperblock[j].size();
                this->newBlocks[i].txns = workingStringVec;
                break;
            }

        }

    }

}

}
