/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinSourceApp
 * DESC: Source File for BitcoinSourceApp.
 */

#include "bitcoinSourceApp.h"

namespace inet{

Define_Module(bitcoinSourceApp);

bitcoinSourceApp::bitcoinSourceApp() {

    // Setup handling for multiple hosts
    // Serve tag 1 first.
    this->receivedFirstVer = false;
    this->isWaitingForFirst = true;
    this->firstTimeLoop = true;

    // Setup new node app bitcoin data (genesis block, etc.)
    this->currentBlockHeight = 1;
    std::string genesisStr = "0000000000000000000000000000000000000000000000000000000000000000";
    this->genesisHash = genesisStr;
    this->genesisStr = "GENESIS";

    // Setup Version and Verack broadcasting and bools from bitcoinApp parent
    this->versionObj = new bitcoinVersionObj("version", 10); // Only have 10 blocks for simulation purposes
    char verackcmd [] = "verack";
    this->verAck = new bitcoinMsgObj("verack", verackcmd);
    this->sentVer = false;
    this->sentVerAck = false;
    this->gotVerAck = false;

    // Get header/Block data from text file created by python generator
    getBlockchainData();

    // Should get a getHeaders message from the new node
    this->gotGetHeaders = false;
    this->sentHeaders = false;

    // Should get a getData msg from the new node
    this->gotGetData = false;
    this->sentGetData = false;

    // Setup bools for script finishing.
    this->isFinished = false;

    // NOTE: This makes the current project work with only 1 node, future work can be done to fix this.
    this->serveQueue.push(0);


    this->servingFirstNode = true;




}

bitcoinSourceApp::~bitcoinSourceApp() {
}

// Initializes application in omnet++ stage
void bitcoinSourceApp::initialize(int stage){

    UdpBasicApp::initialize(stage);
//    socket.setOutputGate(gate("socketOut"));
//    const char *localAddress = par("localAddress");
//    socket.bind(*localAddress ? L3AddressResolver().resolve(localAddress) : L3Address(), localPort);
//    setSocketOptions();

}

// Main function for deciding what data to send new node
void bitcoinSourceApp::sendPacket(){

    if(this->firstTimeLoop){
        this->firstTimeLoop = false;
        this->workingAddr = destAddresses[this->currServIdx];
    }

    // Setup data packet
    std::ostringstream str;
    str << packetName << "-" << numSent;
    Packet *packet = new Packet(str.str().c_str());
    if(dontFragment)
        packet->addTag<FragmentationReq>()->setDontFragment(true);

    const auto& payload = makeShared<ApplicationPacket>();
    payload->setChunkLength(B(par("messageLength")));
    payload->addTag<CreationTimeTag>()->setCreationTime(simTime());
    packet->insertAtBack(payload);

    // End Setup data packet

    // Begin packet data deciding logic

    // Use infinite while loop to create 'script'
    while(true){

        bool hasDatatoSend = false; // Bool for catchall

        // Send dummy msg to initialize socket
        if(!this->sentInitMsg){
            this->sentInitMsg = true;
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("waitmsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "NODE IS INITIALIZING!" << endl;
            break;
        }

        //Handle if haven't received version message from new app
        if(this->receivedFirstVer){
            this->receivedFirstVer = false;
            this->isWaitingForFirst = false;
            // Wait until new nodes send versions.
            hasDatatoSend = true;
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("waitmsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "WAITING FOR FIRST NEW NODE TO CONNECT!" << endl;
            break;
        }

        // Temp, makes source wait until more new nodes send it data...
        if(!this->servingFirstNode && !this->gotVer){
            // Wait until new nodes send versions.
            hasDatatoSend = true;
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("waitmsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "WAITING FOR A NEW NODE TO CONNECT!" << endl;
            break;
        }


        // Send version object initially
        if(!this->sentVer && !this->isWaitingForFirst){
            this->sentVer = true;
            hasDatatoSend = true;
            packet->addObject(this->versionObj);
            EV_INFO << "SENDING VERSION OBJ..."<< endl;
            break;
        }

        // Send verack after getting version packet
        if(!this->sentVerAck && this->gotVer){
            this->sentVerAck = true;
            hasDatatoSend = true;
            packet->addObject(this->verAck);
            EV_INFO << "SENDING VERSION ACK..."<< endl;
            break;
        }

        // Handle sending headers packet.
        if(this->gotGetHeaders && !this->sentHeaders){
            // Send out headers packet
            this->sentHeaders = true;
            hasDatatoSend = true;
            packet->addObject(this->headerDataObj);
            EV_INFO << "SENDING HEADERS!"<< endl;
            break;
        }

        // Handle sending tx data packet.
        if(this->gotGetData && !this->sentGetData){
            // Send out headers packet
            this->sentGetData = true;
            hasDatatoSend = true;
            packet->addObject(this->sendTxnObj);
            this->serveQueue.pop();
            EV_INFO << "SENDING TX DATA!"<< endl;
            if(checkIfFinished()){
                this->isFinished = true; // Source app is finished after this point.
                cDisplayString& displayString = getParentModule()->getDisplayString();
                displayString.setTagArg("i", 0, "misc/sun");
                displayString.setTagArg("is", 1, "s");
            }else{
                emit(packetSentSignal, packet);
                socket.sendTo(packet, this->workingAddr, destPort);
                numSent++;

                // Print out success string to simulation monitor

                EV_INFO << "SOURCE SENT DATA!"<< endl;
                resetDeliveryLoop(); // Reset bools and serve the next node in the queue.
                return;
            }

            break;
        }

        // Handle sending finished packet.
        if(this->isFinished){
            char cmdMsg [] = "finish";
            bitcoinMsgObj *finishMsg = new bitcoinMsgObj("isfinished", cmdMsg);
            packet->addObject(finishMsg);
            EV_INFO << "NODE IS FINISHED!" << endl;
            break;
        }

        // Catchall object to prevent omnet++ from crashing
        if(!hasDatatoSend){
            char cmdMsg [] = "wait";
            bitcoinMsgObj *waitMsg = new bitcoinMsgObj("WaitMsg", cmdMsg);
            packet->addObject(waitMsg);
            EV_INFO << "NODE IS WAITING!" << endl;
            break;
        }

    }

    // End sending packet logic...

    // Get destination address from .ini file and send packet to correct new node

    emit(packetSentSignal, packet);
    socket.sendTo(packet, this->workingAddr, destPort);
    numSent++;

    // Print out success string to simulation monitor

    EV_INFO << "SOURCE SENT DATA!"<< endl;

}

// Called when packets are received...
void bitcoinSourceApp::processPacket(Packet *pk){

    // Begin extracting objects from received packet
    // Use infinite loop as 'script'
    while(true){

        // Handle catchall packet.
        if(pk->hasObject("waitmsg")){
            EV_INFO << "GOT WAIT MSG! DELETING" <<  endl;
            break;
        }

        // Handle finishing packet.
        if(pk->hasObject("isfinished")){
            EV_INFO << "GOT FINISHED SIGNAL FROM NODE DELETING" <<  endl;
            break;
        }

        // Handle getdata packet.
        if(pk->hasObject("getdata")){

            cObject *tempObj = pk->getObject("getdata");
            getDataObj *newgetdata = check_and_cast<getDataObj *>(tempObj);
            EV_INFO << "RECEIVED GETDATA!" <<  endl;
            this->receivedGetData = *newgetdata;
            processGetData();
            this->gotGetData = true;
            break;

        }

        // Handle getheaders packet.
        if(pk->hasObject("getheaders")){
            cObject *tempObj = pk->getObject("getheaders");
            getheadersObj *getheaders = check_and_cast<getheadersObj *>(tempObj);
            EV_INFO << "RECEIVED GETHEADERS: hash_count = " << getheaders->hash_count <<  endl;
            this->receivedGetHeaders = *getheaders;
            processGetHeaders();
            this->gotGetHeaders = true;
            break;
        }

        // Handles verack packet.
        if(pk->hasObject("verack")){
            this->gotVerAck = true;
            EV_INFO << "RECEIVED VERACK FROM NEW NODE" << endl;
            break;
        }

        // Handle version packet.
        if(pk->hasObject("version")){

            cObject *tempObj = pk->getObject("version");
            bitcoinVersionObj *version = check_and_cast<bitcoinVersionObj *>(tempObj);
            if(version->nodeID == this->currServIdx){
                this->receivedFirstVer = true;
                // Established contact with desired node!
                EV_INFO << "RECEIVED VERSION BLOCK: start_height = " << version->start_height <<  endl;
                EV_INFO << "SERVING NEWAPP #" << version->nodeID << endl;
                this->receivedVer = *version;
                this->gotVer = true;
//                 Start delivery loop to first node...
                if(!this->servingFirstNode){
                    this->workingAddr = this->destAddresses[this->currServIdx];
                }


                break;
            }else{
                // Got different nodeID, hold the index and serve in queue.
                EV_INFO << "RECEIVED VERSION BLOCK: start_height = " << version->start_height <<  endl;
                EV_INFO << "WAITING FOR CORRECT HOSTID..." << endl;
                this->serveQueue.push(version->nodeID);
                // Wait until correct node delivers version or is fully satisfied.
                break;
            }

        }else{ // Received weird packet...
            EV_INFO << "SOMETHING WEIRD HAPPENED" << endl;
            break;
        }

    }

    // END HANDLING FOR INCOMING OBJECTS

    EV_INFO << "SOURCE RECEIVED DATA!"<< endl;
    emit(packetReceivedSignal, pk);
    delete pk;
    numReceived++;

}


/*****************Custom Functions************/
// Reads bitcoin data from the csv file
void bitcoinSourceApp::getBlockchainData(){

    // Get Data
    std::string fileName = "bitcoinSimData.csv";
    std::string delimeter = ",";

    std::ifstream file(fileName);

    // C++ vectors are great :D
    std::vector<std::string> dataList;

    std::string value = "";
    // Iterate through each line and split the content using delimeter, gather is large vector for parsing.
    while (file.good()){

        getline(file, value, ',');
        dataList.push_back(value);
    }
    // Close the File
    file.close();

    // Parse into block objects from .csv file in project src directory.

    //    // Global struct copied here for reference
    //    struct block_struct{
    //
    //        int version = 1; // for simulation only
    //        std::string prev_block;
    //        std::string merkle_root;
    //        int timestamp;
    //        int bits; // bits used to calculate target in python code
    //        int nonce;
    //        int txn_count = 2; // 0 for block headers only, 2 for blocks in simulation only
    //        std::vector<std::string> txns;
    //
    //    };

    std::string throwaway;
    int i = 0;
    int csvrowsize = 9;
    for(i = 0; i < dataList.size() - 1;i+=csvrowsize){

        block_struct workingStruct;

        // parse data into block_header struct
        throwaway = dataList[i];
        workingStruct.merkle_root = dataList[i+1];
        workingStruct.curr_hash = dataList[i+2];
        workingStruct.nonce = std::stoi(dataList[i+3]);
        workingStruct.prev_block = dataList[i+4];
        workingStruct.timestamp = std::stoi(dataList[i+5]);
        workingStruct.bits = dataList[i+6];
        workingStruct.txns.push_back(dataList[i+7]);
        workingStruct.txns.push_back(dataList[i+8]);

        this->sourceBlocks.push_back(workingStruct);
        workingStruct.txn_count = 0;
        workingStruct.txns.clear();
        this->sourceHeaders.push_back(workingStruct);

    }

    // At this point, source app should have all the blockchain info it needs.

}

// Called to process the received getheaders object.
void bitcoinSourceApp::processGetHeaders(){

    // Set the number of headers to send in the headers object
    //    this->numHeadersSend = this->receivedGetHeaders.hash_count;
    // --> only do this if new node doesn't need all blocks, which wont be the case for this simulation

    bool sendAllHeaders = false;
    if(this->receivedGetHeaders.hash_stop.compare(this->genesisHash) == 0){ // If recieved hash stop is equal to genesis hash...
        // Get all headers!
        sendAllHeaders = true;
    }else{
        // shouldn't be the case, here in case of future work

    }

    // copy data struct from storage into headers packet
    std::vector<block_struct> tempStruct;

    if(sendAllHeaders){
        this->numHeadersSend = this->sourceHeaders.size();
        int i;

        for(i = 0; i < this->numHeadersSend; i++){
            tempStruct.push_back(this->sourceHeaders[i]);
        }

        std::vector<inet::block_struct> tempinetStruct = tempStruct;
        this->headerDataObj = new headerObj("headers", this->numHeadersSend, tempStruct);

    }else{
        // Shouldn't be the case... (here in case of future work)

    }

}

// Called to process the received getData packet and parse data into the sending trxn object.
void bitcoinSourceApp::processGetData(){

    // Setup trxn vars.
    this->sendTxnObj = new txnObj("txn");
    struct tx_struct temptrxnStruct;
    std::vector<tx_struct> temptrxnVec;

    int i,j;
    for(i = 0; i < this->receivedGetData.inventory.size(); i++){

        temptrxnVec.clear();

        for(j = 0; j < this->sourceBlocks.size(); j++){

            if(this->receivedGetData.inventory[i].hash.compare(this->sourceBlocks[j].curr_hash) == 0){

                this->sendTxnObj->trxHashes.push_back(this->sourceBlocks[j].curr_hash);
                temptrxnStruct.tx_in_out = this->sourceBlocks[j].txns[0];
                temptrxnVec.push_back(temptrxnStruct);
                temptrxnStruct.tx_in_out = this->sourceBlocks[j].txns[1];
                temptrxnVec.push_back(temptrxnStruct);
                this->sendTxnObj->trxnsperblock.push_back(temptrxnVec);
                break;

            }

        }

    }

}

bool bitcoinSourceApp::checkIfFinished(){

    // Check if all new nodes have been served by the source app.
    if(this->serveQueue.empty()){
        return true;
    }else{
        return false;
    }


}

void bitcoinSourceApp::resetDeliveryLoop(){

        int nextNode = this->serveQueue.front();
        this->serveQueue.pop();
        this->workingAddr = destAddresses[nextNode];

        // Setup Version and Verack broadcasting and bools from bitcoinApp parent
        this->versionObj = new bitcoinVersionObj("version", 10); // Only have 10 blocks for simulation purposes
        char verackcmd [] = "verack";
        this->verAck = new bitcoinMsgObj("verack", verackcmd);
        this->sentVer = false;
        this->sentVerAck = false;
        this->gotVerAck = false;

        // Should get a getHeaders message from the new node
        this->gotGetHeaders = false;
        this->sentHeaders = false;

        // Should get a getData msg from the new node
        this->gotGetData = false;
        this->sentGetData = false;
        this->gotVer = false;
        this->servingFirstNode = false;
        this->currServIdx++;
//        this->workingAddr = destAddresses[this->currServIdx];

}



}





