/* NAME: Charles Rawlins
 * DATE: May 5, 2020
 * CLSS: BitcoinApp
 * DESC: Source File for BitcoinApp.
 */

#include "bitcoinApp.h"

namespace inet{

Define_Module(bitcoinApp);

// Default Constructor
bitcoinApp::bitcoinApp(){}

// Default Destructor
bitcoinApp::~bitcoinApp(){}

}


